# Naive script for turning lua/xelatex with unicode-math into plain latex

Writing LaTeX code in unicode is so much more readable than plain
LaTeX that I don't try to resist the temptation anymore.  The downside
is that I often find myself needing to translate a unicode LaTeX file
into plain LaTeX at the last minute when sending the final version of
a paper.

This is a naive script for turning lua/xelatex with unicode-math into
"almost" plain latex, in the sense that it returns a file that should
hopefully not be too far from correct plain latex. It relies on my
usual preamble and coding style, but it might be a useful start for
you in wait for a better solution.

