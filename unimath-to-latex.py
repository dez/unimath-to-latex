
import argparse,subprocess,os,re

def constructReplacements():
    sedfile = open("/home/thirs/unison/dotfiles/unicode-to-latex.sed","r")
    replacements = []
    for line in sedfile:
        items = line.split("/")
        print(items)
        if (len(items) > 3) and (line[0] != "#"):
            before = items[1]
            after = items[2]
            replacements += (before,after)
    replacements += [
        "\\\\bibliographystyle{.*/eptcs}","\\\\bibliographystyle{eptcs}",
            "\\\\bibliography{\\.\\./(.*)}","\\\\bibliography{\\1}",
        "\\\\bibliographystyle{\\.\\./(.*)}","\\\\bibliographystyle{\\1}",
            "\\\\bibliography{\\.\\./(.*)}","\\\\bibliography{\\1}"]
    return replacements

def unicodeToLatex (s, replacements):
    print(replacements)
    for i in range(0,len(replacements),2):
        re.sub(replacements[i],replacements[i+1],s)
    return s

parser = argparse.ArgumentParser()
parser.add_argument("xelatexFile")
args = parser.parse_args()
name = args.xelatexFile
nameOut = name[0:-4] + "-latex.tex"
subprocess.call(["sed -f /home/thirs/unison/dotfiles/unicode-to-latex.sed " + name + " > " + nameOut], shell=True)

print("Opening input file")
f = open(name,"r")
text = ""
for line in f:
    if "%" in line:
        text = text + " " + line
    else:
        text = text + " " + line[:-1]
print("Converting xelatex+unicode to latex")
replacements = constructReplacements()
textOut = unicodeToLatex(text,replacements)
##print("Deleting potential old output file " + nameOut + ".")
##os.remove(nameOut)
print("Creating output file " + nameOut + ".")
fOut = open(nameOut,"w")
print("Writing converted text")
fOut.write(textOut)
print("Closing files")
fOut.close()
f.close()
    
