# Preamble and post-amble stuff
s/\\setmathfont{.*}//
s/\(.*\)-xetex-preamble/\1-preamble/
s/fontspec//
s/newtxmath//
s/etoolbox//
s/unicode-math//
s/\\expandafter\\let\\csname not.*\\endcsname\\relax//g
s/\\let.*\\relax//g
s/\\patchcmd.*2236//g
s/%%% TeX-engine: xetex/%%% TeX-engine: pdflatex/
s/% rubber: module xelatex/% rubber: module pdftex/
s/% rubber: module natbib/% rubber: module bibtex/
s/\\bibliographystyle{\.\.\/\(.*\)}/\\bibliographystyle{\.\/\1}/
s/\\bibliography{\.\.\/\(.*\)}/\\bibliography{\.\/\1}/

# Marche pas
s/\\ExplSyntaxOn.*ExplSyntaxOff//

# Finalisation
s/\\usepackage{}//g
s/\\usepackage{,*}//g

# Commands proper
s/é/\\'e/g
s/è/\\`e/g
s/ê/\\^e/g
s/à/\\`a/g
s/â/\\^a/g
s/î/\\^i/g
s/ô/\^o/g
s/ù/\\`u/g
s/û/\^u/g
s/⃗/^{\\to} /g
s/⃖/^{\\ot} /g
s/̅/^- /g
s/̲/_- /g
s/⌈/\\lceil /g
s/⌉/\\rceil /g
s/⌊/\\lfloor /g
s/⌋/\\rfloor /g
s/⟦/\\llbracket /g
s/⟧/\\rrbracket /g
s/⌜/\\ulcorner /g
s/⌝/\\urcorner /g
s/⦇/\\llparenthesis /g
s/⦈/\\rrparenthesis /g
s/⟪/\\llangle /g
s/⟫/\\rrangle /g
s/⟨/\\langle /g
s/⟩/\\rangle /g
s/≠/\\neq /g
s/∈/\\in /g
s/∉/\\notin /g
s/∋/\\ni /g
s/∌/\\notni /g
s/⊆/\\subseteq /g
s/⊇/\\supseteq /g
s/⊈/\\nsubseteq /g
s/⊉/\\nsupseteq /g
s/⊂/\\subset /g
s/⊃/\\supset /g
s/⊄/\\nsubset /g
s/⊅/\\nsupset /g
s/⊊/\\subsetneq /g
s/⊋/\\supsetneq /g
s/⊏/\\squaresub /g
s/⊐/\\squaresup /g
s/⊑/\\squaresubeq /g
s/⊒/\\squaresupeq /g
s/⋤/\\squaresubnoteq /g
s/⋥/\\squaresupnoteq /g
s/≡/\\equiv /g
s/≢/\\nequiv /g
s/≈/\\approx /g
s/≅/\\cong /g
s/≆/\\ncong /g
s/≃/\\simeq /g
s/≉/\\napprox /g
s/∼/\\sim /g
s/⋔/\\pitchfork /g
s/≤/\\leq /g
s/≥/\\geq /g
s/≔/:= /g
s/≻/\\succ /g
s/≺/\\pred /g
s/≽/\\succeq /g
s/≼/\\predeq /g
s/⊴/\\unlhd /g
s/⊵/\\unrhd /g
s/⊩/\\Vdash /g
s/⊢/\\vdash /g
s/⊨/\\vDash /g
s/⊳/\\triangleright /g
s/▸/\\blacktriangleright /g
s/⊲/\\triangleleft /g
s/⊕/\\oplus /g
s/⊖/\\ominus /g
s/⅋/\\parr /g
s/∪/\\union /g
s/∪/\\cup /g
s/∩/\\inter /g
s/∩/\\cap /g
s/⊓/\\sqcap /g
s/⊔/\\sqcup /g
s/∨/\\vee /g
s/∖/\\setminus /g
s/⊎/\\uplus /g
s/⋅/\\cdot /g
s/⋄/\\diamond /g
s/×/\\times /g
s/÷/\\divide /g
s/⊗/\\otimes /g
s/⊠/\\boxtimes /g
s/⊙/\\odot /g
s/∙/\\bulletop /g
s/∘/\\circ /g
s/∧/\\wedge /g
s/∧/\\gcd /g
s/∗/\\ast /g
s/∥/\\parallel /g
s/⋆/\\star /g
s/↾/\\restriction /g
s/⟹/\\Longrightarrow /g
s/⇒/\\Rightarrow /g
s/⟶/\\longrightarrow /g
s/→/\\rightarrow /g
s/⇐/\\Leftarrow /g
s/⟸/\\Longleftarrow /g
s/←/\\leftarrow /g
s/⟵/\\longleftarrow /g
s/⇔/\\Leftrightarrow /g
s/⟺/\\Longleftrightarrow /g
s/⟷/\\longleftrightarrow /g
s/↔/\\leftrightarrow /g
s/↦/\\mapsto /g
s/⟼/\\varlongmapsto /g
s/↤/\\mapsfrom /g
s/↠/\\epi /g
s/↣/\\mono /g
s/↪/\\hookrightarrow /g
s/↩/\\hookleftarrow /g
s/↼/\\ulharpoon /g
s/⇀/\\urharpoon /g
s/↽/\\dlharpoon /g
s/⇁/\\drharpoon /g
s/↾/\\upharpoonright /g
s/↛/\\nrightarrow /g
s/⇏/\\nRightarrow /g
s/↝/\\leadsto /g
s/↜/\\otsdael /g
s/⊸/\\multimap /g
s/⇉/\\rightrightarrows /g
s/⊢/\\vdash /g
s/⊣/\\dashv /g
s/¬/\\lnot /g
s/¬/\\neg /g
s/↑/\\uparrow /g
s/↓/\\downarrow /g
s/⇑/\\Uparrow /g
s/⇓/\\Downarrow /g
s/⇕/\\Updownarrow /g
s/∀/\\forall /g
s/∃/\\exists /g
s/α/\\alpha /g
s/β/\\beta /g
s/γ/\\gamma /g
s/δ/\\delta /g
s/ϵ/\\varepsilon /g
s/ε/\\epsilon /g
s/ζ/\\zeta /g
s/η/\\eta /g
s/θ/\\theta /g
s/ι/\\iota /g
s/κ/\\kappa /g
s/λ/\\lambda /g
s/μ/\\mu /g
s/ν/\\nu /g
s/ξ/\\xi /g
s/ο/\\omicron /g
s/π/\\pi /g
s/ρ/\\rho /g
s/σ/\\sigma /g
s/τ/\\tau /g
s/υ/\\upsilon /g
s/φ/\\phi /g
s/ϕ/\\varphi /g
s/χ/\\chi /g
s/ψ/\\psi /g
s/ω/\\omega /g
s/Α/\\Alpha /g
s/Β/\\Beta /g
s/Γ/\\Gamma /g
s/Δ/\\Delta /g
s/Ε/\\Epsilon /g
s/Ζ/\\Zeta /g
s/Η/\\Eta /g
s/Θ/\\Theta /g
s/Ι/\\Iota /g
s/Κ/\\Kappa /g
s/Λ/\\Lambda /g
s/Μ/\\Mu /g
s/Ν/\\Nu /g
s/Ξ/\\Xi /g
s/Ο/\\Omicron /g
s/Π/\\Pi /g
s/Ρ/\\Rho /g
s/Σ/\\Sigma /g
s/Τ/\\Tau /g
s/Υ/\\Upsilon /g
s/Φ/\\Phi /g
s/Χ/\\Chi /g
s/Ψ/\\Psi /g
s/Ω/\\Omega /g
s/∇/\\nabla /g
s/𝔄/{\\mathfrak A} /g
s/𝔅/{\\mathfrak B} /g
s/𝔇/{\\mathfrak D} /g
s/𝔈/{\\mathfrak E} /g
s/𝔉/{\\mathfrak F} /g
s/𝔊/{\\mathfrak G} /g
s/𝔍/{\\mathfrak J} /g
s/𝔎/{\\mathfrak K} /g
s/𝔏/{\\mathfrak L} /g
s/𝔐/{\\mathfrak M} /g
s/𝔑/{\\mathfrak N} /g
s/𝔒/{\\mathfrak O} /g
s/𝔓/{\\mathfrak P} /g
s/𝔔/{\\mathfrak Q} /g
s/𝔖/{\\mathfrak S} /g
s/𝔗/{\\mathfrak T} /g
s/𝔘/{\\mathfrak U} /g
s/𝔙/{\\mathfrak V} /g
s/𝔚/{\\mathfrak W} /g
s/𝔛/{\\mathfrak X} /g
s/𝔜/{\\mathfrak Y} /g
s/𝔞/{\\mathfrak a} /g
s/𝔟/{\\mathfrak b} /g
s/𝔠/{\\mathfrak c} /g
s/𝔡/{\\mathfrak d} /g
s/𝔢/{\\mathfrak e} /g
s/𝔣/{\\mathfrak f} /g
s/𝔤/{\\mathfrak g} /g
s/𝔥/{\\mathfrak h} /g
s/𝔦/{\\mathfrak i} /g
s/𝔧/{\\mathfrak j} /g
s/𝔨/{\\mathfrak k} /g
s/𝔩/{\\mathfrak l} /g
s/𝔪/{\\mathfrak m} /g
s/𝔫/{\\mathfrak n} /g
s/𝔬/{\\mathfrak o} /g
s/𝔭/{\\mathfrak p} /g
s/𝔮/{\\mathfrak q} /g
s/𝔯/{\\mathfrak r} /g
s/𝔰/{\\mathfrak s} /g
s/𝔱/{\\mathfrak t} /g
s/𝔲/{\\mathfrak u} /g
s/𝔳/{\\mathfrak v} /g
s/𝔴/{\\mathfrak w} /g
s/𝔵/{\\mathfrak x} /g
s/𝔶/{\\mathfrak y} /g
s/𝔷/{\\mathfrak z} /g
s/♯/\\sharp /g
s/♮/\\natural /g
s/♭/\\flat /g
s/∞/\\infty /g
s/∂/\\partial /g
s/⊤/\\top /g
s/⊥/\\bottom /g
s/⊥/\\bot /g
s/∅/\\emptyset /g
s/…/\\dots /g
s/…/... /g
s/⋮/\\vdots /g
s/—/\\wc /g
s/♥/\\heartsuit /g
s/⋯/\\cdots /g
s/□/\\wbox /g
s/□/\\wsquare /g
s/■/\\bbox /g
s/■/\\bsquare /g
s/✓/\\checkmark /g
s/•/\\bullet /g
s/†/\\dagger /g
s/ℵ/\\alef /g
s/ℶ/\\bet /g
s/ℷ/\\gimel /g
s/ℸ/\\dalet /g
s/ℓ/\\ell /g
s/𝔸/{\\mathbb A} /g
s/𝔹/{\\mathbb B} /g
s/ℂ/{\\mathbb C} /g
s/𝔻/{\\mathbb D} /g
s/𝔼/{\\mathbb E} /g
s/𝔽/{\\mathbb F} /g
s/𝔾/{\\mathbb G} /g
s/ℍ/{\\mathbb H} /g
s/𝕀/{\\mathbb I} /g
s/𝕁/{\\mathbb J} /g
s/𝕂/{\\mathbb K} /g
s/𝕃/{\\mathbb L} /g
s/𝕄/{\\mathbb M} /g
s/ℕ/{\\mathbb N} /g
s/𝕆/{\\mathbb O} /g
s/ℙ/{\\mathbb P} /g
s/ℚ/{\\mathbb Q} /g
s/ℝ/{\\mathbb R} /g
s/𝕊/{\\mathbb S} /g
s/𝕋/{\\mathbb T} /g
s/𝕌/{\\mathbb U} /g
s/𝕍/{\\mathbb V} /g
s/𝕎/{\\mathbb W} /g
s/𝕏/{\\mathbb X} /g
s/𝕐/{\\mathbb Y} /g
s/ℤ/{\\mathbb Z} /g
s/𝟘/{\\mathbb 0} /g
s/𝟙/{\\mathbb 1} /g
s/𝟚/{\\mathbb 2} /g
s/𝟛/{\\mathbb 3} /g
s/𝟜/{\\mathbb 4} /g
s/𝟝/{\\mathbb 5} /g
s/𝟞/{\\mathbb 6} /g
s/𝟟/{\\mathbb 7} /g
s/𝟠/{\\mathbb 8} /g
s/𝟡/{\\mathbb 9} /g
s/𝒜/{\\mathcal A} /g
s/ℬ/{\\mathcal B} /g
s/𝒞/{\\mathcal C} /g
s/𝒟/{\\mathcal D} /g
s/ℰ/{\\mathcal E} /g
s/ℱ/{\\mathcal F} /g
s/𝒢/{\\mathcal G} /g
s/ℋ/{\\mathcal H} /g
s/ℐ/{\\mathcal I} /g
s/𝒥/{\\mathcal J} /g
s/𝒦/{\\mathcal K} /g
s/ℒ/{\\mathcal L} /g
s/ℳ/{\\mathcal M} /g
s/𝒩/{\\mathcal N} /g
s/𝒪/{\\mathcal O} /g
s/𝒫/{\\mathcal P} /g
s/𝒬/{\\mathcal Q} /g
s/ℛ/{\\mathcal R} /g
s/𝒮/{\\mathcal S} /g
s/𝒯/{\\mathcal T} /g
s/𝒰/{\\mathcal U} /g
s/𝒱/{\\mathcal V} /g
s/𝒲/{\\mathcal W} /g
s/𝒳/{\\mathcal X} /g
s/𝒴/{\\mathcal Y} /g
s/𝒵/{\\mathcal Z} /g
s/∑/\\sum /g
s/∏/\\prod /g
s/∐/\\coprod /g
s/∫/\\int /g
s/∬/\\dint /g
s/∭/\\tint /g
s/⨌/\\qint /g
s/∮/\\oint /g
s/⋂/\\biginter /g
s/⋂/\\bigcap /g
s/⋃/\\bigunion /g
s/⋃/\\bigcup /g
s/⋁/\\bigvee /g
s/⋁/\\bigsup /g
s/⋀/\\bigwedge /g
s/⋀/\\biginf /g
s/⨁/\\bigoplus /g
s/⨂/\\bigotimes /g
s/★/\\star /g
s/𝐀/{\\mathbf A} /g
s/𝐁/{\\mathbf B} /g
s/𝐂/{\\mathbf C} /g
s/𝐃/{\\mathbf D} /g
s/𝐄/{\\mathbf E} /g
s/𝐅/{\\mathbf F} /g
s/𝐆/{\\mathbf G} /g
s/𝐇/{\\mathbf H} /g
s/𝐈/{\\mathbf I} /g
s/𝐉/{\\mathbf J} /g
s/𝐊/{\\mathbf K} /g
s/𝐋/{\\mathbf L} /g
s/𝐌/{\\mathbf M} /g
s/𝐍/{\\mathbf N} /g
s/𝐎/{\\mathbf O} /g
s/𝐏/{\\mathbf P} /g
s/𝐐/{\\mathbf Q} /g
s/𝐑/{\\mathbf R} /g
s/𝐒/{\\mathbf S} /g
s/𝐓/{\\mathbf T} /g
s/𝐔/{\\mathbf U} /g
s/𝐕/{\\mathbf V} /g
s/𝐖/{\\mathbf W} /g
s/𝐗/{\\mathbf X} /g
s/𝐘/{\\mathbf Y} /g
s/𝐙/{\\mathbf Z} /g
s/𝐚/{\\mathbf a} /g
s/𝐛/{\\mathbf b} /g
s/𝐜/{\\mathbf c} /g
s/𝐝/{\\mathbf d} /g
s/𝐞/{\\mathbf e} /g
s/𝐟/{\\mathbf f} /g
s/𝐠/{\\mathbf g} /g
s/𝐡/{\\mathbf h} /g
s/𝐢/{\\mathbf i} /g
s/𝐣/{\\mathbf j} /g
s/𝐤/{\\mathbf k} /g
s/𝐥/{\\mathbf l} /g
s/𝐦/{\\mathbf m} /g
s/𝐧/{\\mathbf n} /g
s/𝐨/{\\mathbf o} /g
s/𝐩/{\\mathbf p} /g
s/𝐪/{\\mathbf q} /g
s/𝐫/{\\mathbf r} /g
s/𝐬/{\\mathbf s} /g
s/𝐭/{\\mathbf t} /g
s/𝐮/{\\mathbf u} /g
s/𝐯/{\\mathbf v} /g
s/𝐰/{\\mathbf w} /g
s/𝐱/{\\mathbf x} /g
s/𝐲/{\\mathbf y} /g
s/𝐳/{\\mathbf z} /g
s/ⱼ/_j /g
s/ₜ/_t /g
s/ₛ/_s /g
s/ₚ/_p /g
s/ₙ/_n /g
s/ₘ/_m /g
s/ₗ/_l /g
s/ₖ/_k /g
s/ₕ/_h /g
s/ₓ/_x /g
s/ₒ/_o /g
s/ₑ/_e /g
s/ₐ/_a /g
s/₎/_) /g
s/₍/_( /g
s/₌/_\\eq /g
s/₊/_+ /g
s/₉/_9 /g
s/₈/_8 /g
s/₇/_7 /g
s/₆/_6 /g
s/₅/_5 /g
s/₄/_4 /g
s/₃/_3 /g
s/₂/_2 /g
s/₁/_1 /g
s/₀/_0 /g
s/ᵪ/_\\chi /g
s/ᵩ/_\\phi /g
s/ᵨ/_\\rho /g
s/ᵧ/_\\gamma /g
s/ᵦ/_\\beta /g
s/ᵥ/_v /g
s/ᵤ/_u /g
s/ᵣ/_r /g
s/ᵢ/_i /g
s/ⱽ/^V /g
s/ⁿ/^n /g
s/⁾/^) /g
s/⁽/^( /g
s/⁼/^\\eq /g
s/⁺/^+ /g
s/⁹/^9 /g
s/⁸/^8 /g
s/⁷/^7 /g
s/⁶/^6 /g
s/⁵/^5 /g
s/⁴/^4 /g
s/ⁱ/^i /g
s/⁰/^0 /g
s/ᶿ/^\\theta /g
s/ᶻ/^z /g
s/ᶠ/^f /g
s/ᶜ/^c /g
s/ᵡ/^\\chi /g
s/ᵠ/^\\phi /g
s/ᵟ/^\\delta /g
s/ᵞ/^\\gamma /g
s/ᵝ/^\\beta /g
s/ᵛ/^v /g
s/ᵘ/^u /g
s/ᵗ/^t /g
s/ᵖ/^p /g
s/ᵒ/^o /g
s/ᵐ/^m /g
s/ᵏ/^k /g
s/ᵍ/^g /g
s/ᵉ/^e /g
s/ᵈ/^d /g
s/ᵇ/^b /g
s/ᵃ/^a /g
s/ᵂ/^W /g
s/ᵁ/^U /g
s/ᵀ/^T /g
s/ᴿ/^R /g
s/ᴾ/^P /g
s/ᴼ/^O /g
s/ᴺ/^N /g
s/ᴹ/^M /g
s/ᴸ/^L /g
s/ᴷ/^K /g
s/ᴶ/^J /g
s/ᴵ/^I /g
s/ᴴ/^H /g
s/ᴳ/^G /g
s/ᴱ/^E /g
s/ᴰ/^D /g
s/ᴮ/^B /g
s/ᴬ/^A /g
s/ˣ/^x /g
s/ˢ/^s /g
s/ˡ/^l /g
s/ʸ/^y /g
s/ʷ/^w /g
s/ʳ/^r /g
s/ʲ/^j /g
s/ʰ/^h /g
s/º/^o /g
s/¹/^1 /g
s/³/^3 /g
s/²/^2 /g
s/ª/^a /g
s/⌜/^\\ulcorner /g
s/⌝/^\\urcorner /g
s/⋈/\\bowtie /g
s/⫫/\\bbot /g
s/≁/\\nsim /g
s/⋊/\\rtimes /g
s/⧄/\\boxslash /g
s/∶/\\mathrel{:} /g
s/∷/\\Colon /g
s/⫪/\\ttop /g
s/§/\\partie /g
s/̲/\\underline /g
s/̅/\\overline /g
s/̊/\\interior /g
s/̇/\\dotabove /g
