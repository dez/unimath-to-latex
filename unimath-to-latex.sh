
function unicode-to-latex () {
    u=${1##*/}
    s=${u%.*}
    sed -f unimath-to-latex.sed $1 > $s-latex.tex
    python unimath-to-mathit.py $s-latex.tex
}
